## Role Variables

* `monitoring_domain`: `` - the domain name at which the monitoring endpoints will be exposed

**@**
  ```yaml
 monitoring_domain: monitoring.example.com
 ```

* `monitoring_nfs_mount_dir`: `/srv/monitoring` - The NFS point mount on the server



* `monitoring_nfs_url`: `none` - the NFS url to ensure is mounted, if not defined, no nfs mount is used



* `monitoring_nfs_options`: `defaults,vers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport,_netdev` - the NFS mount options



* `monitoring_blackbox_exporter_enabled`: `true` - whether to deploy the blackbox exporter



* `monitoring_cloudflared_enabled`: `false` - whether to deploy cloudflared to expose the endpoints over Argo Tunnel



* `alertmanager_receivers`: `[]` - A list of notification receivers. Configuration same as in [official docs](https://prometheus.io/docs/alerting/configuration/



* `alertmanager_route`: `{}` - Alert routing. More in [official docs](https://prometheus.io/docs/alerting/configuration/



* `grafana_dashboards`: `[]` - List of dashboards which should be imported



* `grafana_datasource_extra`: `[]` - List of extra datasources which should be configured (prometheus is automatically added)



* `prometheus_scrape_configs`: `none` - Prometheus scrape jobs provided in same format as in [official docs](https://prometheus.io/docs/prometheus/latest/configuration/configuration/


